%define _unpackaged_files_terminate_build 1
%define pypi_name pure-protobuf
%define mod_name pure_protobuf

%def_with check

Name: python3-module-%pypi_name
Version: 3.1.0
Release: alt1
Summary: Python implementation of Protocol Buffers with dataclass-based schemas
License: MIT
Group: Development/Python3
Url: https://pypi.org/project/pure-protobuf
Vcs: https://github.com/eigenein/protobuf.git
BuildArch: noarch

Source0: %name-%version.tar
Source1: %pyproject_deps_config_name

%pyproject_runtimedeps_metadata
BuildRequires(pre): rpm-build-pyproject
%pyproject_builddeps_build

%if_with check
%add_pyproject_deps_check_filter mkdocs*
%pyproject_builddeps_metadata
%pyproject_builddeps_check
%endif

%description
pure-protobuf leverages the standard dataclasses module for defining
message types, offering a modern alternative to the traditional approach.
This method is recommended for new projects due to its simplicity and
integration with Python's features. Compatible with Python 3.6
and later versions, the dataclasses interface streamlines the process of
structuring your data. While the legacy interface remains accessible
through pure_protobuf.legacy, it is considered deprecated and is not
recommended for future use. This documentation aims to guide you in
utilizing pure-protobuf effectively, aligning closely with the standard
developer guide and presupposing a basic understanding of Protocol Buffers.

%prep
%setup
%pyproject_scm_init
%pyproject_deps_resync_build
%pyproject_deps_resync_metadata
%if_with check
%pyproject_deps_resync_check_poetry
%endif

%build
%pyproject_build

%install
%pyproject_install

%check
%pyproject_run_pytest -ra --benchmark-disable -o=addopts=''

%files
%doc LICENSE README.md CONTRIBUTING.md
%python3_sitelibdir/%mod_name
%python3_sitelibdir/%{pyproject_distinfo %mod_name}

%changelog
* Fri Jun 21 2024 Aleksandr A. Voyt <sobue@altlinux.org> 3.1.0-alt1
- Initial commit
